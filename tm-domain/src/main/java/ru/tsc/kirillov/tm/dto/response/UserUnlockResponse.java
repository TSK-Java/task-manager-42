package ru.tsc.kirillov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractResultResponse {
}

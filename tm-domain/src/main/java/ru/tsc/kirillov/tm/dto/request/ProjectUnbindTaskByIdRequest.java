package ru.tsc.kirillov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectUnbindTaskByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    public ProjectUnbindTaskByIdRequest(
            @Nullable final String token,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        super(token);
        this.projectId = projectId;
        this.taskId = taskId;
    }

}

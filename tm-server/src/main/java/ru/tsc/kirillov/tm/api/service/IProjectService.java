package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.ProjectDTO;

public interface IProjectService extends IUserOwnedService<ProjectDTO> {

    @NotNull
    String[] findAllId(@Nullable final String userId);

}

package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.api.service.IProjectTaskService;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.dto.model.ProjectDTO;
import ru.tsc.kirillov.tm.dto.model.TaskDTO;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kirillov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    public ProjectTaskService(
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    private void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId,
            boolean isAdd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectService.existsByIdUserId(userId, projectId)) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneByIdUserId(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(isAdd ? projectId : null);
        taskService.update(task);
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        bindTaskToProject(userId, projectId, taskId, true);
    }

    @Override
    public void unbindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        bindTaskToProject(userId, projectId, taskId, false);
    }

    @Nullable
    @Override
    public ProjectDTO removeProjectById(@Nullable final String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        taskService.removeAllByProjectId(userId, projectId);
        projectService.removeByIdUserId(userId, projectId);
        return null;
    }

    @Nullable
    @Override
    public ProjectDTO removeProjectByIndex(@Nullable final String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final ProjectDTO project = projectService.findOneByIndexUserId(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return removeProjectById(userId, project.getId());
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull String[] projects = projectService.findAllId(userId);
        for (@NotNull final String projectId : projects)
            if (!projectId.isEmpty())
                taskService.removeAllByProjectId(userId, projectId);
        projectService.clearUserId(userId);
    }

}
